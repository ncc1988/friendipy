from django.core.exceptions import ObjectDoesNotExist


from friendipy.core.profile import get_user_profile
from friendipy.pages.main import Page
from friendipy.models import Profile
from friendipy.classes.widgets import FormWidget
from friendipy.forms.profile_forms import BasicProfileEditForm
from friendipy.forms.settings_forms import ProfileSettingsForm, ProfileThemeSettingsForm
from friendipy.classes.pseudo_widgets import SettingsPagesWidget


class SettingsPage(Page):
    def __init__(self, user=None):
        super(SettingsPage, self).__init__(user=user)
        self.widgets.append(SettingsPagesWidget())


class ProfileThemeSettingsPage(Page):
    def __init__(self, user=None):
        super(ProfileThemeSettingsPage, self).__init__(user=user)
        self.widgets.append(FormWidget(ProfileThemeSettingsForm(profile=get_user_profile(user))))
        self.side_widgets.append(SettingsPagesWidget())

class ProfileDataSettingsPage(Page):
    def __init__(self, user=None):
        super(ProfileDataSettingsPage, self).__init__(user=user)
        self.side_widgets.append(SettingsPagesWidget())
        try:
            self.widgets.append(FormWidget(BasicProfileEditForm(instance=user.profile)))
        except Profile.DoesNotExist:
            user.profile = Profile()
            user.profile.save()
            self.widgets.append(FormWidget(BasicProfileEditForm(instance=user.profile)))
            