from django.contrib.auth.models import User
from django.core.urlresolvers import reverse


from friendipy.models import *
from friendipy.classes.content import TextContent, LinkContent, HtmlContent, FormContent, ImageContent
from friendipy.classes.nav import NavItem
from friendipy.classes.widgets import *
from friendipy.classes.pseudo_widgets import *

from friendipy.core.profile import get_user_profile
from friendipy.core.posts import collect_posts_from_contact, collect_posts_by_date
from friendipy.core.themes import get_user_theme_name, get_profile_color_scheme, get_theme_default_color_scheme

from friendipy.forms.authentication_forms import *
from friendipy.forms.friend_forms import *
from friendipy.forms.post_forms import *

from friendipy.protocols.dfrn.widgets import write_header as dfrn_write_header



def create_standard_nav_bar(additional_items = None, user=None):
    """Builds the standard navigation bar with optional additional items
    
    
    Keyword arguments:
    additional_items -- the additional navigation bar items as list of NavItem
    user -- an (optional) Django User object (for loggedin users)
    """
    nav_menu = []
    nav_menu.append(LinkContent(action=reverse('friendipy-timeline'), title='friendipy')) #friendipy logo
    
    nav_menu.append(SpaceContent())
    
    if(isinstance(user, User)):
        #the user is logged in
        try:
            #we just want to test if there is a profile:
            user.profile
        except Profile.DoesNotExist:
            #if the user has no profile we should add one:
            user.profile = Profile()
            user.profile.save()
        
        user_menu_entries = [
            LinkContent(action=reverse('friendipy-contacts'), title='Your contacts'),
            LinkContent(action=reverse('friendipy-profile', args=(user.id,)), title='Your profile'),
            LinkContent(action=reverse('friendipy-settings_start'), title='Settings')
            ]
        menu_icon = ImageContent(source=user.profile.photo_url, description=user.profile.name)
        nav_menu.append(MenuContent(entries=user_menu_entries, title=user.profile.name, icon=menu_icon))
        
        nav_menu.append(SpaceContent())
        nav_menu.append(LinkContent(action=reverse('friendipy-logout'), title='Logout'))
        nav_menu.append(TextContent(text=user.username+'@'+'localhost', size='small')) #TODO: remove localhost with real server name!
    else:
        #user is not logged in: add register button
        nav_menu.append(LinkContent(action=reverse('friendipy-register'), title='Register'))
        
    return nav_menu #STUB



class PageSettings(object):
    """
    This class holds all settings that can safely be passed to a template file
    without revealing sensitive information (like a user's password).
    """
    def __init__(self):
        self.theme_name = ''
        self.color_scheme_name = ''
        



class Page(object):
    def __init__(self,
                 user=None,
                 title='friendipy',
                 header_content = None,
                 nav_items = None,
                 side_widgets = None,
                 widgets = None,
                 message_list = None):
        """
        If user is None then it is an unauthenticated user!
        """
        self.user = None
        if(user != None):
            #if(not isinstance(user, User)):
            #    raise Exception("Page constructor: User instance has to be None or a Django User instance!")
            self.user = user
        
        self.title = title
        #nav items are always the same:
        self.nav_items = create_standard_nav_bar(user=self.user, additional_items = nav_items)
        
        """
        header_content is for content that is written in the HTML header.
        TODO: Restrict this to link, script, style and meta!
        """
        
        self.header_content = list()
        
        if(header_content != None):
            if(isinstance(header_content, list)):
                self.header_content = header_content #concat the two lists
        
        if(side_widgets != None):
            self.side_widgets = side_widgets #list of Widget
        else:
            self.side_widgets = list()
        if(widgets != None):
            self.widgets = widgets #list of Widget
        else:
            self.widgets = list()
        if(message_list != None):
            self.message_list = message_list
        else:
            self.message_list = list()
        #print("DEBUG: Page.__init__: widgets="+str(widgets))
        
        self.settings = PageSettings()
        self.settings.theme_name = get_user_theme_name(self.user) #this already catches all exceptions
        
        
        try:
            self.settings.color_scheme_name = get_profile_color_scheme(get_user_profile(self.user))
        except Exception:
            self.settings.color_scheme_name = get_theme_default_color_scheme() #use the default theme's default color scheme
        #print("DEBUG: Page color_scheme_name = "+str(self.settings.color_scheme_name))


class MainPage(Page):
    def __init__(self, user=None):
        super(MainPage, self).__init__(user=user) #the super class constructor also checks if user is a Django User object
        
        if(self.user != None):
            #only authenticated users have their name in the title
            self.title = user.username + ' - friendipy'
        welcome_widget = Widget(title='Welcome, '+self.user.username)
        welcome_widget.content.append(TextContent("Would you like to add a Friendica user?"))
        welcome_widget.content.append(FormContent(AddFriendForm(), action=reverse('add_friend'), method='post', submit_value='add'))
        
        self.widgets.append(welcome_widget)


class LoginPage(Page):
    def __init__(self, user=None):
        super(LoginPage, self).__init__(user=user)
        self.title = 'friendipy login'
        login_widget = Widget(title='Welcome to friendipy')
        login_widget.content.append(TextContent('If you don\'t have a users account yet you may register at this friendipy instance to connect to your friends on Friendica and other social networks.'))
        login_widget.content.append(FormContent(LoginForm(), action=reverse('friendipy-login'), method='post', submit_value='login'))
        self.widgets.append(login_widget)
        
        login_widget.content.append(TextContent(text='Still having no friendipy user account?'))
        login_widget.content.append(LinkContent(action=reverse('friendipy-register'), title='register now'))

class RegistrationPage(Page):
    def __init__(self, user=None, message_list=None):
        super(RegistrationPage, self).__init__(message_list=message_list)
        self.title = 'Register at this friendipy instance'
        registration_widget = Widget(title='Register at this friendipy instance')
        registration_widget.content.append(FormContent(RegistrationForm(), action=reverse('friendipy-register'), method='post', submit_value='register'))
        self.widgets.append(registration_widget)


class ContactPage(Page):
    def __init__(self, user=None):
        super(ContactPage, self).__init__(user=user)
        
        if(self.user != None):
            #only authenticated users can see this page!"
            self.title = user.username + ' - friendipy'
            welcome_widget = Widget(title='Your contacts')
            welcome_widget.content.append(TextContent("Would you like to add someone to your contact list?"))
            welcome_widget.content.append(FormContent(AddFriendForm(), action=reverse('friendipy-contacts'), method='post', submit_value='add'))
            
            self.widgets.append(welcome_widget)
            self.widgets.append(ContactItemsWidget(user))

class AddFriendResultPage(Page):
    def __init__(self, user=None, profile=None):
        if(not isinstance(profile, Profile)):
            raise Exception("Error: AddFriendResultPage requires a friendipy Profile class instance!")
        
        super(AddFriendResultPage, self).__init__(user=user)
        widget = ProfileWidget(profile)
        widget.content.append(FormContent(form=AddFriendForm({'profile_url':profile.url}), submit_value='Add', action=reverse("friendipy-add_contact")))
        self.widgets.append(widget)


class ProfilePage(Page):
    def __init__(self, user=None, profile=None, posts=None):
        """
        Requests a certain profile.
        Note: user may be not the same as profile.user.
        user is taken from request.user while profile can represent the profile of another user.
        """
        super(ProfilePage, self).__init__(user=user)
        self.title = profile.name
        self.header_content.append(HtmlContent(html_code=dfrn_write_header(profile=profile)))
        self.widgets.append(ProfileWidget(profile=profile, show_protocol_profiles=True))
        if(posts != None):
            #posts shall also be displayed below the profile:
            for p in posts:
                self.widgets.append(PostWidget(post))


class ContactProfilePage(Page):
    def __init__(self, user=None, profile=None):
        """
        Requests a certain profile.
        Note: user may be not the same as profile.user.
        user is taken from request.user while profile can represent the profile of another user.
        """
        super(ContactProfilePage, self).__init__(user=user)
        self.title = profile.name
        self.widgets.append(ProfileWidget(profile=profile, show_protocol_profiles=True))
        posts = None
        try:
            posts = collect_posts_from_contact(profile)
            
            if(posts != None):
                #posts shall also be displayed below the profile:
                for p in posts:
                    self.widgets.append(PostWidget(p))
        except Exception as e:
            print("ContactProfilePage: Couldn't read contact's posts: "+str(e))




class TimelinePage(Page):
    def __init__(self, user=None):
        super(TimelinePage, self).__init__(user=user)
        
        profile = get_user_profile(user)
        self.side_widgets.append(ProfileWidget(profile=profile))
        
        new_post_widget = Widget()
        new_post_widget.content.append(FormContent(NewPostForm(), action=reverse('friendipy-new_post'), method='post', submit_value='share'))
        
        self.widgets.append(new_post_widget)
        
        #now load the user's posts (if any):
        #posts = profile.post_set.all().order_by('-creation_date')
        posts = collect_posts_by_date(profile)
        if(posts != None):
            for post in posts:
                self.widgets.append(PostWidget(post))
        
        