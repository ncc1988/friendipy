# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('friendipy', '0010_post_checksum'),
    ]

    operations = [
        migrations.CreateModel(
            name='ProfileSettings',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, verbose_name='ID', primary_key=True)),
                ('profile', models.OneToOneField(to='friendipy.Profile', null=True, related_name='settings')),
                ('theme', models.ForeignKey(null=True, blank=True, to='friendipy.Theme')),
            ],
        ),
        migrations.RemoveField(
            model_name='usersettings',
            name='theme',
        ),
        migrations.RemoveField(
            model_name='usersettings',
            name='user',
        ),
        migrations.RemoveField(
            model_name='themesetting',
            name='user_settings',
        ),
        migrations.DeleteModel(
            name='UserSettings',
        ),
        migrations.AddField(
            model_name='themesetting',
            name='profile_settings',
            field=models.ForeignKey(null=True, related_name='theme_settings', to='friendipy.ProfileSettings'),
        ),
    ]
