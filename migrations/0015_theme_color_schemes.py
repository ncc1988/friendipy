# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('friendipy', '0014_auto_20160410_1843'),
    ]

    operations = [
        migrations.AddField(
            model_name='theme',
            name='color_schemes',
            field=models.CharField(max_length=2048, default='default'),
        ),
    ]
