# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('friendipy', '0002_auto_20151218_1316'),
    ]

    operations = [
        migrations.CreateModel(
            name='Event',
            fields=[
                ('item_ptr', models.OneToOneField(primary_key=True, parent_link=True, auto_created=True, to='friendipy.Item', serialize=False)),
                ('location', models.CharField(max_length=255)),
                ('start_date', models.DateTimeField(default=datetime.datetime(1970, 1, 1, 0, 0))),
                ('end_date', models.DateTimeField(blank=True, null=True)),
                ('creator', models.ForeignKey(to='friendipy.Profile')),
            ],
            options={
            },
            bases=('friendipy.item',),
        ),
        migrations.CreateModel(
            name='Position',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', auto_created=True, serialize=False)),
                ('latitude', models.FloatField()),
                ('longitude', models.FloatField()),
                ('altitude', models.FloatField(default=0.0)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='event',
            name='exact_position',
            field=models.ForeignKey(to='friendipy.Position', null=True),
            preserve_default=True,
        ),
    ]
