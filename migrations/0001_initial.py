# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Contact',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', auto_created=True, serialize=False)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ContactGroup',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', auto_created=True, serialize=False)),
                ('name', models.CharField(max_length=64)),
                ('members', models.ManyToManyField(to='friendipy.Contact')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='FilePermission',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', auto_created=True, serialize=False)),
                ('access', models.BooleanField(default=True)),
                ('contact', models.ForeignKey(to='friendipy.Contact')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Item',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', auto_created=True, serialize=False)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Mark',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', auto_created=True, serialize=False)),
                ('verb', models.CharField(max_length=24)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Post',
            fields=[
                ('item_ptr', models.OneToOneField(auto_created=True, to='friendipy.Item', primary_key=True, parent_link=True, serialize=False)),
                ('creation_date', models.DateTimeField(blank=True, default=datetime.datetime(1970, 1, 1, 0, 0))),
                ('title', models.CharField(blank=True, null=True, max_length=255)),
                ('body', models.TextField()),
            ],
            options={
            },
            bases=('friendipy.item',),
        ),
        migrations.CreateModel(
            name='Profile',
            fields=[
                ('item_ptr', models.OneToOneField(auto_created=True, to='friendipy.Item', primary_key=True, parent_link=True, serialize=False)),
                ('url', models.URLField(blank=True, max_length=512, null=True, default=None, unique=True)),
                ('protocol', models.CharField(default='dfrn', max_length=16)),
                ('last_update', models.DateTimeField(blank=True, default=datetime.datetime(1970, 1, 1, 0, 0))),
                ('name', models.CharField(blank=True, null=True, default='', max_length=255)),
                ('nickname', models.CharField(blank=True, null=True, default='', max_length=255)),
                ('location', models.CharField(blank=True, null=True, default='', max_length=64)),
                ('address', models.TextField(blank=True, null=True, default='', max_length=256)),
                ('country', models.CharField(blank=True, null=True, default='', max_length=64)),
                ('description', models.TextField(blank=True, null=True, default='', max_length=1024)),
                ('photo_url', models.URLField(blank=True, null=True, default='', max_length=512)),
                ('date_of_birth', models.DateField(null=True)),
                ('gender', models.CharField(blank=True, null=True, default='', max_length=32)),
                ('public_key', models.TextField(blank=True, null=True, default='')),
                ('homepage', models.URLField(blank=True, null=True, default='', max_length=512)),
                ('readonly', models.BooleanField(default=False)),
                ('forum', models.BooleanField(default=False)),
            ],
            options={
            },
            bases=('friendipy.item',),
        ),
        migrations.CreateModel(
            name='Tag',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', auto_created=True, serialize=False)),
                ('name', models.CharField(max_length=64)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Theme',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', auto_created=True, serialize=False)),
                ('name', models.CharField(max_length=24)),
                ('description', models.TextField()),
                ('version', models.CharField(max_length=12)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ThemeSetting',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', auto_created=True, serialize=False)),
                ('key', models.CharField(max_length=32)),
                ('value', models.CharField(max_length=128)),
                ('theme', models.ForeignKey(to='friendipy.Theme')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='UploadedFile',
            fields=[
                ('item_ptr', models.OneToOneField(auto_created=True, to='friendipy.Item', primary_key=True, parent_link=True, serialize=False)),
                ('name', models.CharField(max_length=255)),
                ('file_type', models.PositiveIntegerField(default=0)),
                ('mime', models.CharField(max_length=64)),
                ('size', models.PositiveIntegerField()),
                ('data', models.BinaryField()),
                ('sha256sum', models.CharField(max_length=65)),
                ('creation_time', models.DateTimeField()),
                ('modification_time', models.DateTimeField()),
                ('public_access', models.BooleanField(default=False)),
            ],
            options={
            },
            bases=('friendipy.item',),
        ),
        migrations.CreateModel(
            name='UserSettings',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', auto_created=True, serialize=False)),
                ('theme', models.ForeignKey(to='friendipy.Theme', blank=True, null=True)),
                ('user', models.OneToOneField(to=settings.AUTH_USER_MODEL, related_name='settings')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='themesetting',
            name='user_settings',
            field=models.ForeignKey(to='friendipy.UserSettings', related_name='theme_settings'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='profile',
            name='tags',
            field=models.ManyToManyField(blank=True, to='friendipy.Tag', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='profile',
            name='user',
            field=models.OneToOneField(to=settings.AUTH_USER_MODEL, blank=True, null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='post',
            name='attachments',
            field=models.ManyToManyField(blank=True, to='friendipy.UploadedFile', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='post',
            name='creator',
            field=models.ForeignKey(to='friendipy.Profile'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='post',
            name='parent',
            field=models.ForeignKey(to='friendipy.Post', blank=True, null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='post',
            name='references',
            field=models.ManyToManyField(blank=True, to='friendipy.Contact', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='post',
            name='tags',
            field=models.ManyToManyField(blank=True, to='friendipy.Tag', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='mark',
            name='item',
            field=models.ManyToManyField(related_name='items', to='friendipy.Item', null=True, default=None),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='mark',
            name='profile',
            field=models.ManyToManyField(to='friendipy.Profile'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='filepermission',
            name='f',
            field=models.ForeignKey(to='friendipy.UploadedFile'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='contact',
            name='profile',
            field=models.OneToOneField(to='friendipy.Profile'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='contact',
            name='user',
            field=models.OneToOneField(to=settings.AUTH_USER_MODEL),
            preserve_default=True,
        ),
    ]
