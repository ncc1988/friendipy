# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('friendipy', '0012_auto_20160410_1822'),
    ]

    operations = [
        migrations.AddField(
            model_name='theme',
            name='default_color_scheme',
            field=models.CharField(max_length=32, default='default'),
        ),
        migrations.AlterField(
            model_name='profilesettings',
            name='theme',
            field=models.ForeignKey(null=True, to='friendipy.Theme', blank=True),
        ),
    ]
