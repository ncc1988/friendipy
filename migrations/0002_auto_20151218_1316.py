# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('friendipy', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='mark',
            name='item',
            field=models.ManyToManyField(related_name='items', to='friendipy.Item'),
            preserve_default=True,
        ),
    ]
