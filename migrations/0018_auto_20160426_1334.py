# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('friendipy', '0017_globalsetting'),
    ]

    operations = [
        migrations.AlterField(
            model_name='globalsetting',
            name='key',
            field=models.CharField(unique=True, max_length=48, db_index=True),
        ),
    ]
