# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('friendipy', '0009_auto_20160402_1538'),
    ]

    operations = [
        migrations.AddField(
            model_name='post',
            name='checksum',
            field=models.CharField(default='', blank=True, max_length=96),
        ),
    ]
