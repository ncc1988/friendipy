# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('friendipy', '0005_auto_20160109_2057'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='profile',
            name='contact_groups',
        ),
        migrations.AddField(
            model_name='contactgroup',
            name='profile',
            field=models.ForeignKey(related_name='contact_groups', default=1, to='friendipy.Profile'),
            preserve_default=False,
        ),
    ]
