# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('friendipy', '0011_auto_20160410_1757'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profilesettings',
            name='profile',
            field=models.OneToOneField(to='friendipy.Profile', related_name='settings'),
        ),
        migrations.AlterField(
            model_name='profilesettings',
            name='theme',
            field=models.ForeignKey(to='friendipy.Theme', default='default', null=True, blank=True),
        ),
    ]
