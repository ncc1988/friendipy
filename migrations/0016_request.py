# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('friendipy', '0015_theme_color_schemes'),
    ]

    operations = [
        migrations.CreateModel(
            name='Request',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, serialize=False, verbose_name='ID')),
                ('request_type', models.CharField(max_length=32)),
                ('remote_uri', models.URLField(max_length=512)),
                ('target_profile', models.ForeignKey(to='friendipy.Profile')),
            ],
        ),
    ]
