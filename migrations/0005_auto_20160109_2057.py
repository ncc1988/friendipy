# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('friendipy', '0004_profile_contacts'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='contact',
            name='profile',
        ),
        migrations.RemoveField(
            model_name='contact',
            name='user',
        ),
        migrations.RemoveField(
            model_name='filepermission',
            name='contact',
        ),
        migrations.RemoveField(
            model_name='profile',
            name='contacts',
        ),
        migrations.AddField(
            model_name='filepermission',
            name='profile',
            field=models.ForeignKey(default=1, to='friendipy.Profile'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='profile',
            name='contact_groups',
            field=models.ManyToManyField(to='friendipy.ContactGroup'),
        ),
        migrations.AlterField(
            model_name='contactgroup',
            name='members',
            field=models.ManyToManyField(related_name='member_in_groups', to='friendipy.Profile'),
        ),
        migrations.AlterField(
            model_name='post',
            name='attachments',
            field=models.ManyToManyField(to='friendipy.UploadedFile', blank=True),
        ),
        migrations.AlterField(
            model_name='post',
            name='creator',
            field=models.ForeignKey(related_name='own_posts', to='friendipy.Profile'),
        ),
        migrations.AlterField(
            model_name='post',
            name='references',
            field=models.ManyToManyField(related_name='referenced_posts', to='friendipy.Profile', blank=True),
        ),
        migrations.AlterField(
            model_name='post',
            name='tags',
            field=models.ManyToManyField(to='friendipy.Tag', blank=True),
        ),
        migrations.AlterField(
            model_name='profile',
            name='tags',
            field=models.ManyToManyField(to='friendipy.Tag', blank=True),
        ),
        migrations.DeleteModel(
            name='Contact',
        ),
    ]
