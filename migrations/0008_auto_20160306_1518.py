# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('friendipy', '0007_auto_20160109_2305'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='private_key',
            field=models.TextField(blank=True, default='', null=True),
        ),
        migrations.AlterField(
            model_name='event',
            name='start_date',
            field=models.DateTimeField(default=datetime.datetime(1970, 1, 1, 1, 0)),
        ),
        migrations.AlterField(
            model_name='post',
            name='creation_date',
            field=models.DateTimeField(blank=True, default=datetime.datetime(1970, 1, 1, 1, 0)),
        ),
        migrations.AlterField(
            model_name='profile',
            name='last_update',
            field=models.DateTimeField(blank=True, default=datetime.datetime(1970, 1, 1, 1, 0)),
        ),
    ]
