# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('friendipy', '0008_auto_20160306_1518'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='public_post_url',
            field=models.URLField(null=True, max_length=512, blank=True, default=None),
        ),
        migrations.AlterField(
            model_name='event',
            name='start_date',
            field=models.DateTimeField(default=datetime.datetime(1970, 1, 1, 0, 0)),
        ),
        migrations.AlterField(
            model_name='post',
            name='creation_date',
            field=models.DateTimeField(blank=True, default=datetime.datetime(1970, 1, 1, 0, 0)),
        ),
        migrations.AlterField(
            model_name='profile',
            name='last_update',
            field=models.DateTimeField(blank=True, default=datetime.datetime(1970, 1, 1, 0, 0)),
        ),
        migrations.AlterField(
            model_name='profile',
            name='protocol',
            field=models.CharField(max_length=24, default='friendipy'),
        ),
    ]
