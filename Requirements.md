You will need the following to install and use Friendipy:
- Python 3
- Django >= 1.7

Required python modules:
- binascii
- bs4
- feedparser
- requests
- rsa
- pyasn1
- lxml


NOTE for OpenSUSE users:
[2016-04-06] The requests module has to be installed via pip, the OpenSUSE package (python3-requests) is not working.
