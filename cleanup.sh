#!/bin/sh

rm -rf ./forms/__pycache__ 2>/dev/null
rm -r ./pages/__pycache__ 2>/dev/null
rm -r ./migrations/__pycache__ 2>/dev/null
rm -r ./__pycache__ 2>/dev/null
rm -r ./templatetags/__pycache__ 2>/dev/null
rm -r ./classes/__pycache__ 2>/dev/null
rm -r ./protocols/__pycache__ 2>/dev/null
rm -r ./functions/__pycache__ 2>/dev/null


echo "DONE!"
