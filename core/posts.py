
import datetime
import feedparser
import time
import pytz

from django.db.models import Q

from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth.models import User

from friendipy.models import Profile, Post
from friendipy.core.profile_read_functions import get_profile_urls
from friendipy.core.contacts import get_profile_contacts

"""
This file contains functions related to posts.
"""


def collect_posts_by_date(profile=None, limit = 25):
    if(profile == None):
        raise Exception('Error: collect_posts_by_date: profile not set!')
    
    #first get the profile's own posts:
    posts = list()
    try:
        posts += list(profile.own_posts.filter(parent=None).order_by('-creation_date'))
    except Post.DoesNotExist:
        return None
    
    #then the posts from the profile's contacts:
    
    contacts = get_profile_contacts(profile)
    #print('DEBUG: Contacts:'+str(contacts))
    
    if(contacts):
        for contact in contacts:
            #print('DEBUG: contact posts:'+str(contact.own_posts.all()))
            #local contacts:
            posts += list(contact.own_posts.filter(parent=None).order_by('-creation_date'))
            #remote contacts:
            posts += list(collect_posts_from_contact(contact))
            
    #now sort them all according to the creation date and return them:
    
    posts = sorted(posts, key=lambda p: p.creation_date, reverse=True)
    
    #print("DEBUG: Posts:"+str(posts))
    #for p in posts:
    #    print(str(p))
    return posts[:limit]


def read_public_posts(profile=None, min_date_filter=None, max_date_filter=None):
    if(not isinstance(profile, Profile)):
        raise Exception("read_public_posts_from_url requires a Friendipy Profile instance as contact!")
    #TODO: implement min_date_filter and max_date_filter
    
    if(profile.public_post_url != None):
        #print("Reading public posts for profile "+str(profile)+" from URL "+str(profile.public_post_url)+"...")
        feed = feedparser.parse(profile.public_post_url)
        posts = []
        #print("posts="+str(posts))
        
        for entry in feed['entries']:
            p = Post()
            p.creator = profile
            p.title = entry['title']
            #print("EntryTitle="+str(p.title))
            for c in entry['content']:
                p.body += c.value
            p.creation_date = datetime.datetime.fromtimestamp(time.mktime(entry.published_parsed)).replace(tzinfo=pytz.UTC)
            posts.append(p)
        return posts
    else:
        raise Exception("read_public_posts_from_url: Profile has no valid public_post_url (or it is None)!")
    

def collect_posts_from_contact(contact=None, sort=False):
    if(not isinstance(contact, Profile)):
        raise Exception("Error: collect_posts_from_contacts requires a Friendipy Profile instance!")
    
    #for testing only:
    #contact.public_post_url = None
    #contact.save()
    
    if(contact.public_post_url == None):
        """
        We don't have a public post URL yet.
        We should retrieve it, except when the protocol does not support it!
        """
        print("Retrieving profile urls for contact "+str(contact)+" (ID="+str(contact.id)+")...")
        get_profile_urls(contact)
    
    #first we get the public posts
    posts = read_public_posts(contact)
    
    #then the private ones
    #(TODO)
    
    #then we sort them by date (if sort==True):
    #(TODO)
    
    #and finally return them:
    return posts


def store_posts_from_contact(contact=None):
    posts = collect_posts_from_contact(contact=contact, sort=True)
    
    for p in posts:
        p.save()
    
    
def update_posts_from_contacts():
    """
    Searches the database for all profile instances that have a user connected to it.
    This way, only contacts from local users are updated.
    """
    
    #with this request only those non-local-user profiles that are members in a contact group are selected
    contact_profiles = Profile.objects.filter(Q(user = None) & ~Q(member_in_groups = None))
    
    print("DEBUG: update_posts_from_contacts: contact_profiles = "+str(contact_profiles))
    
    for contact in contact_profiles:
        store_posts_from_contact(contact)
    