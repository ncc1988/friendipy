"""
All stuff related to contacts belongs here.
"""


def get_profile_contacts(profile=None):
    if(profile == None):
        return None
    
    contact_groups = profile.contact_groups.all()
    contacts = None
    
    for group in contact_groups:
        if(contacts == None):
            contacts = group.members.all()
        else:
            contacts |= group.members.all()
    
    return contacts


def is_in_contact_list(profile=None, contact=None):
    if(profile == None):
        return False
    if(contact == None):
        return False
    
    if(profile.contact_groups):
        for group in profile.contact_groups.all():
            if(contact in group.members.all()):
                return True
    
    return False