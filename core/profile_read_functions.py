
import bs4
from datetime import datetime
import requests

from friendipy.models import Profile
from friendipy.protocols.webfinger import probe_host as webfinger_probe_host, ResourceDescription


def get_profile_urls(profile=None):
    """
    profile is a Profile instance whose URLs have to be retrieved.
    """
    if(not isinstance(profile, Profile)):
        raise Exception("get_profile_urls: profile has to be a Friendipy Profile instance!")
    
    rd = webfinger_probe_host(profile.url)
    
    feed_link = [feed_link for feed_link in rd.links if feed_link.rel == 'http://schemas.google.com/g/2010#updates-from'][0]
    print("Profile "+str(profile)+" has public post url "+str(feed_link.href))
    profile.public_post_url = feed_link.href
    profile.save()
    

    
def read_profile_from_hcard(hcard_url=None):
    """
    If a hcard URL is given try to extract as much information as possible from it
    and store it into a Profile instance.
    """
    if(not isinstance(hcard_url, str)):
        raise Exception("Error: read_profile_from_hcard requires an URL string!")
    response = requests.get(hcard_url)
    page_content = bs4.BeautifulSoup(response.text)
    profile = Profile()
    
    profile.last_update = datetime.now()
    
    
    profile.protocol = 'friendica/diaspora'
    
    try:
        profile.name = page_content.select('.fn')[0].get_text()
    except Exception:
        print("NOTE: name not defined!")
        profile.name = ''
    try:
        profile.nickname = page_content.select('.nickname')[0].get_text()
    except Exception:
        print("NOTE: nickname not defined!")
        profile.nickname = ''
    
    try:
        profile.public_key = page_content.select('.key')[0].get_text()
    except Exception:
        print("NOTE: public key not defined!")
        profile.public_key = ''
    
    try:
        profile.photo_url = page_content.select('.photo')[0].get('src')
    except Exception:
        profile.photo_url = None
    try:
        profile.location = page_content.select('.locality')[0].get_text()
    except Exception:
        profile.location = None
    try:
        profile.country = page_content.select('.country-name')[0].get_text()
    except Exception:
        profile.country = None
    try:
        profile.description = page_content.select('.about')[0].get_text()
    except Exception:
        profile.description = None
    try:
        profile.homepage = page_content.select('.homepage-url')[0].get_text()
    except Exception:
        profile.homepage = None
    return profile
