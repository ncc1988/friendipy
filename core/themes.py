from django.contrib.auth.models import User

from friendipy.models import Profile, ProfileSettings, ThemeSetting, Theme
from friendipy.core.profile import get_user_profile, get_profile_settings


def get_default_theme():
    """
    This returns the instance of the default theme, if set.
    """
    default_theme = None
    try:
        default_theme = Theme.objects.get(name='default')
    except Theme.DoesNotExist:
        raise Exception("Default theme not set! Check your installation! (or run python3 manage.py loaddata friendipy)")
    return default_theme


def get_profile_theme_name(profile=None):
    if(not isinstance (profile, Profile)):
        #the profile does not exist: Use the default theme:
        return 'default'
    
    try:
        #try to find out the profile's theme name, if set by the user:
        return profile.settings.theme_settings.theme.name
    except Exception:
        #something went wrong: use the default theme name
        return 'default'


def get_profile_theme(profile=None):
    if(not isinstance (profile, Profile)):
        #the profile does not exist: No theme!
        return None
    
    try:
        #try to find out the profile's theme, if set by the user:
        return profile.settings.theme_settings.theme
    except Exception:
        #something went wrong: no theme!
        return None


def get_user_theme_name(user=None):
    """
    This returns a theme name even if the user is not logged in.
    """
    if(not isinstance(user, User)):
        return get_profile_theme_name()
    else:
        try:
            return get_profile_theme_name(get_user_profile(user))
        except Exception:
            """
            Something went wrong: Use the default theme name.
            """
            return get_profile_theme_name()


def get_theme_color_schemes(theme = None):
    """
    Returns an array with the names of the color schemes defined in a theme.
    If no theme object is given, the color scheme names 
    of the default theme are returned.
    """
    if(not isinstance(theme, Theme)):
        #the color schemes of the default theme:
        return ['Bostalsee', 'Development', 'Terminal', 'Voelklingen', 'Waldhoelzbach']
    
    color_schemes = theme.color_schemes.strip().split(',')
    #TODO: check for control characters and other characters that may be dangerous!
    return color_schemes


def get_theme_default_color_scheme(theme = None):
    if(not isinstance(theme, Theme)):
        return 'Bostalsee' #the default color scheme of the default theme
    else:
        return theme.default_color_scheme


def get_theme_setting(profile_settings=None, key=None):
    """
    A function to get a key from the ThemeSetting table
    which is used as a key-value store for theme settings.
    This function requires a ProfileSettings object.
    """
    if(not isinstance(profile_settings, ProfileSettings)):
        raise Exception("get_theme_setting requires that profile_settings is a ProfileSettings instance!")
    
    """
    We try to go straight to the value of the requested key.
    Depending on the key the try..catch block
    in the function calling this function has to
    react in an appropriate manner for the key.
    """
    try:
        return profile_settings.theme_settings.filter(
            theme = profile_settings.theme, 
            key = key)[0]
    except IndexError:
        """
        No ThemeSetting instances found in database:
        """
        return None


def get_profile_color_scheme(profile = None):
    """
    Get the color scheme that is associated with a user's profile.
    Due to the fact that Friendipy does (will) support mapping
    multiple users to one profile instance, color schemes
    are associated with a profile rather than a user instance.
    """
    if(not isinstance(profile, Profile)):
        raise Exception("get_profile_color_scheme: profile has to be a Friendipy Profile instance!")
    
    color_scheme_name = None
    
    try:
        color_scheme = get_theme_setting(get_profile_settings(profile), 'color_scheme')
        if(color_scheme.value == None):
            #we don't want undefined values for color schemes!
            color_scheme.value = get_theme_default_color_scheme(profile_settings.theme)
            color_scheme.save()
        
            
    except Exception:
        """
        No theme setting with key=='color_scheme' by now.
        We're asking for the default color scheme and store that one:
        """
        profile_settings = get_profile_settings(profile)
        theme_setting = ThemeSetting()
        theme_setting.profile_settings = profile_settings
        if(profile_settings.theme == None):
            """
            The user has not set a theme yet:
            set the default theme!
            """
            profile_settings.theme = get_default_theme()
            profile_settings.save()
        
        theme_setting.theme = profile_settings.theme
        theme_setting.key = 'color_scheme'
        theme_setting.value = get_theme_default_color_scheme(profile_settings.theme)
        theme_setting.save()
        color_scheme = theme_setting
        
    return color_scheme.value


def set_profile_color_scheme(profile=None, color_scheme_index=None):
    if(not isinstance(profile, Profile)):
        raise Exception("get_profile_color_scheme: profile has to be a Friendipy Profile instance!")
    
    color_scheme_index = int(color_scheme_index)
    
    profile_settings = get_profile_settings(profile)
    theme_color_schemes = get_theme_color_schemes(profile_settings.theme)
    #print("theme_color_schemes = "+str(theme_color_schemes))
    color_scheme_setting = None
    try:
        color_scheme_setting = get_theme_setting(profile_settings, 'color_scheme')
        theme_color_schemes = get_theme_color_schemes(color_scheme_setting.theme)
        #we don't want undefined values for color schemes!
        if(color_scheme_index < len(theme_color_schemes)):
            color_scheme_setting.value = theme_color_schemes[color_scheme_index]
            color_scheme_setting.save()
            #print("DEBUG: set color scheme to: "+str(color_scheme_setting.value))
    except Exception:
        """
        No theme setting with key=='color_scheme' by now.
        We're storing a new key-value pair:
        """
        if(color_scheme_index < len(theme_color_schemes)):
            color_scheme_setting = ThemeSetting()
            color_scheme_setting.profile_settings = profile_settings
            color_scheme_setting.theme = profile_settings.theme
            color_scheme_setting.key = 'color_scheme'
            color_scheme_setting.value = theme_color_schemes[color_scheme_index]
            color_scheme_setting.save()
            #print("DEBUG: set color scheme to: "+str(color_scheme_setting.value))
        
    if(color_scheme_setting == None):
        #setting not found: create it
        if(color_scheme_index < len(theme_color_schemes)):
            color_scheme_setting = ThemeSetting()
            color_scheme_setting.profile_settings = profile_settings
            color_scheme_setting.theme = profile_settings.theme
            color_scheme_setting.key = 'color_scheme'
            color_scheme_setting.value = theme_color_schemes[color_scheme_index]
            color_scheme_setting.save()
            #print("DEBUG: set color scheme to: "+str(color_scheme_setting.value))