from friendipy.models import GlobalSetting


def get_global_setting(key = None, value = None):
    """
    Returns the value of a given setting key and sets its value
    if the key is not set.
    """
    
    if(key == None):
        raise Exception("get_global_setting: key is not set!")
    
    try:
        setting = GlobalSetting.objects.get(key=key).value
    except GlobalSetting.DoesNotExist:
        g = GlobalSetting()
        g.key = key
        g.value = value
        g.save()
        setting = g.value
    
    return setting
