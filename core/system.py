from friendipy.models import GlobalSetting
from friendipy.core.settings import get_global_setting


def build_url_prefix():
    """
    Build the URL prefix by looking at GlobalSetting objects
    and create missing GlobalSetting objects if they don't exist yet.
    """
    domain = get_global_setting('domain', 'localhost')
    use_https = int(get_global_setting('use_https', 'False'))
    
    #print("DEBUG: domain = "+domain)
    #print("DEBUG: use_https = "+str(use_https))
    if(use_https == 1):
        return 'https://'+domain
    else:
        return 'http://'+domain


def query_https_enabled():
    if(get_global_setting('use_https', 'False') == 'True'):
        return True
    else:
        return False


def get_domain_name():
    """
    This returns the domain name of this instance, if set. If it is not set
    it defaults to 'localhost' and stores this in the database.
    """
    return get_global_setting('domain', 'localhost')

    