
import bs4
import email
import json
import requests
import rsa


from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
from django.core.urlresolvers import reverse

from friendipy.models import Profile, ProfileSettings
from friendipy.protocols.dfrn.functions import read_profile_from_url as dfrn_read_profile_from_url
from friendipy.protocols.webfinger import probe_host as webfinger_probe_host, ResourceDescription
from friendipy.core.profile_read_functions import read_profile_from_hcard



def get_user_profile(user=None):
    if(user == None):
        raise Exception("get_user_profile: No user specified!")
    
    if(not isinstance(user, User)):
        raise Exception("get_user_profile: user must be a Django User instance!")
    
    try:
        #we just want to test if there is a profile:
        user.profile
    except Profile.DoesNotExist:
        #if the user has no profile we should add one:
        user.profile = Profile()
        user.profile.save()
    
    check_profile_key_pair(user.profile)
    
    return user.profile


def get_profile_settings(profile=None):
    if(not isinstance(profile, Profile)):
        raise Exception("get_profile_settings: profile must be a Friendipy Profile instance!")
    
    try:
        return profile.settings
    except ProfileSettings.DoesNotExist:
        profile_settings = ProfileSettings()
        profile_settings.profile = profile
        profile_settings.save()
        return profile.settings #if this works the profile_settings object was saved successfully


def check_profile_key_pair(profile=None):
    """
    If the key pair (profile.public_key and profile.private_key) are not set
    or invalid they will be generated here.
    """
    if(isinstance(profile, Profile)):
        if((profile.private_key != '') and (profile.public_key != '')):
            #check if both keys are valid (you can't have the one without the other)
            pass
        else:
            #keys not present: generate them
            keypair = rsa.newkeys(256, poolsize=1) #for testing purposes only 256 bits are used
            profile.public_key = str(keypair[0].save_pkcs1(format='PEM'))
            profile.private_key = str(keypair[1].save_pkcs1(format='PEM'))
            profile.save()
    
    else:
        raise Exception("check_profile_key_pair: profile is not a Profile instance!")



def read_profile_from_uri(uri=None):
    """
    This function analyzes the URI and then determines
    how it should be treated. It returns a profile
    or raises an exception if no profile can be read.
    """
    
    """
    check if the URI has an E-Mail like syntax
    (a list with two items should be the result):
    """
    
    #first we will try WebFinger:
    try:
        resource_description = webfinger_probe_host(uri)
        profile_url = resource_description.subject.split(':', 1)[1] #subject has the scheme acct:user@example.org
        hcard_link = None
        profile=None
        
        hcard_link = [hcard_link for hcard_link in resource_description.links if hcard_link.rel == 'http://microformats.org/profile/hcard'][0]
        #print(str(hcard_link))
        if(hcard_link != None):
            #hcard link present: access it:
            profile = read_profile_from_hcard(hcard_link.href)
            profile.url = profile_url
            return profile
        else:
            raise Exception("No profile could be read via WebFinger!")
    except Exception as e:

        print("DEBUG: WebFinger request failed: "+str(e))
        
        """
        Now we must try other formats:
        It can be a Friendica profile (direct DFRN),
        an RSS/Atom feed or an OStatus profile page.
        """
        
        #first we try DFRN:
        profile = None
        try:
            print("DEBUG: Trying to read DFRN profile from: "+str(uri))
            profile = dfrn_read_profile_from_url(uri)
            profile.url = uri
            return profile
        except Exception:
            print("DEBUG: DFRN profile reading failed!")
            #then we try RSS/Atom
            #(todo)
            raise Exception("Unsupported profile page!")
    