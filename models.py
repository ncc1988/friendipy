from datetime import datetime

from django.db import models
from django.contrib.auth.models import User


from enum import Enum


#new database model, derived from Friendica database model


class Item(models.Model):
    pass


#class Profile(models.Model):
class Profile(Item):
    """ 
    The user's profile: unlike in Friendica
    a user can have only one profile.
    
    If there is no reference to a user object then 
    the profile must belong to a user on another Friendica/Friendipy instance.
    """
    user = models.OneToOneField(User, null=True, blank=True)
    """
    For local users this is not set.
    For remote users this is the URL of the profile
    """
    url = models.URLField(max_length=512, unique=True, null=True, blank=True, default=None) #FIXME: this is problematic: only one user can have a null value!
    
    """
    The following fields save some network requests,
    because the URLs don't have to be retrieved every time.
    If one of these fields is set to '' instead of None then there is no URL for that feature.
    """
    public_post_url = models.URLField(max_length=512, null=True, blank=True, default=None)
    
    
    # the protocol name in lowercase letters (for extern users)
    protocol = models.CharField(max_length=24, default='friendipy')
    
    
    """
    We need to know when a profile was updated the last time!
    """
    last_update = models.DateTimeField(null=False, blank=True, default=datetime.fromtimestamp(0))
    
    #basic profile data (optional):
    name = models.CharField(max_length=255, null=True, blank=True, default='')
    nickname = models.CharField(max_length=255, null=True, blank=True, default='')
    location = models.CharField(max_length=64, null=True, blank=True, default='')
    address = models.TextField(max_length=256, null=True, blank=True, default='')
    country = models.CharField(max_length=64, null=True, blank=True, default='')
    description = models.TextField(max_length=1024, null=True, blank=True, default='')
    tags = models.ManyToManyField('Tag', blank=True)
    photo_url = models.URLField(max_length=512, null=True, blank=True, default='')
    
    #extended data (optional):
    date_of_birth = models.DateField(null=True)
    
    gender = models.CharField(max_length=32, null=True, blank=True, default='') #let the user write this by him-/herself
    private_key = models.TextField(null=True, blank=True, default='')
    public_key = models.TextField(null=True, blank=True, default='')
    homepage = models.URLField(max_length=512, null=True, blank=True, default='')
    
    #profile status data:
    readonly = models.BooleanField(default=False) #if the profile is locked
    forum = models.BooleanField(default=False) #if the profile is a forum
    
    def __str__(self):
        if(self.user):
            return self.user.username
        elif(self.url):
            return self.url
        else:
            return 'UNCONNECTED'
    
    def update(self, other):
        if(other.url == self.url):
            #it is the same profile
            if(other.last_update > self.last_update):
                #the other profile is newer
                if(other.last_update):
                    self.last_update = other.last_update
                if(other.name):
                    self.name = other.name
                if(other.nickname):
                    self.nickname = other.nickname
                if(other.location):
                    self.location = other.location
                if(other.address):
                    self.address = other.address
                if(other.country):
                    self.country = other.country
                if(other.description):
                    self.description = other.description
                if(other.tags):
                    #TODO: check if tags exist
                    self.tags = other.tags
                if(other.photo_url):
                    self.photo_url = other.photo_url
                if(other.date_of_birth):
                    self.date_of_birth = other.date_of_birth
                if(other.gender):
                    self.gender = other.gender
                if(other.public_key):
                    self.public_key = other.public_key
                if(other.homepage):
                    self.homepage = other.homepage
                self.readonly = other.readonly
                self.forum = other.forum
        else:
            raise Exception("Profile update: Non-equal profiles (url fields don't match!)")
    
    
    
class ProfileSettings(models.Model):
    """
    Profile settings are stored in here.
    Due to the fact that Friendipy does (will) support mapping
    multiple users to one profile instance, settings
    are associated with a profile rather than a user instance.
    """
    profile = models.OneToOneField(Profile, related_name='settings')
    theme = models.ForeignKey('Theme', null=True, blank=True) #the user selected theme

  

class ContactGroup(models.Model):
    #this is named group in Friendica
    profile = models.ForeignKey('Profile', related_name='contact_groups')
    name = models.CharField(max_length=64)
    members = models.ManyToManyField('Profile', related_name='member_in_groups')


#class UploadedFile(models.Model):
class UploadedFile(Item):
    #attached and uploaded files
    name = models.CharField(max_length=255)
    file_type = models.PositiveIntegerField(default=0)
    mime = models.CharField(max_length=64)
    size = models.PositiveIntegerField()
    data = models.BinaryField()
    sha256sum = models.CharField(max_length=65) #sum + \0 character
    creation_time = models.DateTimeField()
    modification_time = models.DateTimeField()
    public_access = models.BooleanField(default=False) #if this is set to True the file is public.
                                                        #(except for those contacts that explicitly don't have the permission)
                                                        #Otherwise the file is private and only those contacts with explicit access permission may access it.


class Mark(models.Model):
    #mark can be verbs ("like", "dislike", "love", "thank"), not just like
    verb = models.CharField(max_length=24)
    #posts = models.ManyToManyField('Post') #a contact can set a verb on many posts and one post can have many marks
    item = models.ManyToManyField('Item', related_name='items') #a profile can set a verb on many items and one item can have many marks
    profile = models.ManyToManyField('Profile') #a profile can set many marks at one post, but a post can also be marked with the same verb by many users


class FilePermission(models.Model):
    profile = models.ForeignKey('Profile')
    f = models.ForeignKey(UploadedFile)
    access = models.BooleanField(default=True, blank=True) #the user has permission to access the file only if this is set to True
    #if access is set to false the access is denied. If no rule exists for a user then the access is denied

#class Post(models.Model):
class Post(Item):
    creator = models.ForeignKey('Profile', related_name='own_posts') #we can't use User here, because otherwise posts from other Friendica/Friendipy instances can't be handled
    parent = models.ForeignKey('Post', null=True, blank=True, related_name='threads') #a comment is a post's child node
    creation_date = models.DateTimeField(default=datetime.fromtimestamp(0), blank=True)
    title = models.CharField(max_length=255, null=True, blank=True)
    body = models.TextField() #BBCode is in here
    attachments = models.ManyToManyField('UploadedFile', blank=True) #there can be more than one file attached to a post
    tags = models.ManyToManyField('Tag', blank=True)
    references = models.ManyToManyField('Profile', blank=True, related_name='referenced_posts') #references to profile (@-annotations)
    
    checksum = models.CharField(max_length=96, default='', blank=True)
    
    def __str__(self):
        return self.creator.name + ': ' + str(self.title)


  
class Tag(models.Model):
    name = models.CharField(max_length=64)
    
    def __str__(self):
        return self.name

class Theme(models.Model):
    """
    For installed themes.
    """
    name = models.CharField(max_length=24, unique=True) #the name shouldn't be too long
    description = models.TextField()
    version = models.CharField(max_length=12) #should be enough for a version string
    
    #theme attributes:
    color_schemes = models.CharField(max_length=2048, default='default') #a comma separated list of color schemes
    default_color_scheme = models.CharField(max_length=32, default='default')
    
    def __str__(self):
        return str(self.name)
    

class ThemeSetting(models.Model):
    """
    Theme specific settings belong here
    """
    profile_settings = models.ForeignKey(ProfileSettings, related_name='theme_settings', null=True)
    theme = models.ForeignKey('Theme')
    key = models.CharField(max_length=32)
    value = models.CharField(max_length=128)
    
    def __str__(self):
        return str(self.profile_settings.profile.name)+": " + str(self.theme.name) + ": " + self.key + " = " + self.value


class Position(models.Model):
    #stores positions from a global coordinate system
    latitude = models.FloatField() #in degrees
    longitude = models.FloatField() #in degrees
    altitude = models.FloatField(default=0.0) #in meter; This field can be omitted

class Event(Item):
    creator = models.ForeignKey('Profile') #we store all events we can gather (even from remote users)
    location = models.CharField(max_length=255) #where does the event take place?
    exact_position = models.ForeignKey('Position', null=True) #optional: the location's lat, lon and alt
    start_date = models.DateTimeField(default=datetime.fromtimestamp(0)) #when does the event start?
    end_date = models.DateTimeField(null=True, blank=True) #when does it end? (if applicable)




class Request(models.Model):
    
    """
    This class is for protocol-specific requests of any kind.
    """
    
    """
    request_type specifies what kind of request this is.
    Possible values:
    'dfrn.request'  (Friendica "add" request)
    'friendipy.report' (report inappropriate content)
    (more to be added)
    remote_uri is the URI/URL that needs to be visited to finish the request
    target_profile is the local profile where the request is pointing to
    """
    request_type = models.CharField(max_length=32, null=False)
    
    remote_uri = models.URLField(max_length=512, null=False)
    target_profile = models.ForeignKey(Profile)
    
    


class GlobalSetting(models.Model):
    """
    This class is required to store the global settings of a Friendipy instance.
    """
    
    key = models.CharField(max_length=48, null=False, blank=False, unique=True, db_index=True)
    value = models.CharField(max_length=512, null=True, blank=True)
    
    
    def __str__(self):
        return self.key + ' = ' + str(self.value)
    