from django.contrib import admin

from friendipy.models import *

# Register your models here.

admin.site.register(Profile)
admin.site.register(ProfileSettings)
admin.site.register(ThemeSetting)
admin.site.register(ContactGroup)
admin.site.register(UploadedFile)
admin.site.register(Mark)
admin.site.register(FilePermission)
admin.site.register(Post)
admin.site.register(Tag)
admin.site.register(Theme)
admin.site.register(GlobalSetting)
