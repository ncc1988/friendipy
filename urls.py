#
#   This file is part of friendipy
#   Copyright (C) 2015-2016 Moritz Strohm <ncc1988@posteo.de>
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as
#   published by the Free Software Foundation, either version 3 of the
#   License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU Affero General Public License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from django.conf.urls import patterns, include, url

urlpatterns = patterns('',
                url(r'^$', 'friendipy.views.timeline', name='friendipy-timeline'),
                url(r'^login', 'friendipy.views.login', name='friendipy-login'),
                url(r'^logout', 'friendipy.views.logout', name='friendipy-logout'),
                url(r'^register', 'friendipy.views.register', name='friendipy-register'),
                
                url(r'^contact/(\d+)/$', 'friendipy.views.show_contact', name='friendipy-show_contact'),
                url(r'^contacts', 'friendipy.views.contacts', name='friendipy-contacts'),
                
                
                #TODO: keep friendipy-profile in sync with classes.pseudo_widgets.ContactItemsWidget
                url(r'^profile/(\d+)/$', 'friendipy.views.show_profile', name='friendipy-profile'),
                url(r'^action/new_post', 'friendipy.views.new_post', name='friendipy-new_post'),
                url(r'^action/add_contact', 'friendipy.views.add_contact', name='friendipy-add_contact'),
                
                url(r'^settings/profile', 'friendipy.views.settings_profile', name='friendipy-settings_profile'),
                url(r'^settings/theme', 'friendipy.views.settings_theme', name='friendipy-settings_theme'),
                url(r'^settings/', 'friendipy.views.settings_start', name='friendipy-settings_start'),
                
                url(r'^raw_profile_photo/(\d+)/$', 'friendipy.views.raw_profile_photo', name='friendipy-raw_profile_photo'),
                
                #WebFinger protocol part and related:
                url(r'^.well-known/host-meta', 'friendipy.protocols.webfinger.host_meta', name='friendipy-webfinger_host_meta'),
                url(r'^.well-known/webfinger', 'friendipy.protocols.webfinger.query', name='friendipy-webfinger_query'),
                                
                #XRD:
                url(r'^part/xrd/acct\:(?P<profile_id>[0-9A-Za-z]+)\@(?P<domain>[A-Za-z0-9\.]+)', 'friendipy.documents.xrd.views.xrd', name='friendipy-xrd'),
                url(r'^part/xrd/', 'friendipy.documents.xrd.views.xrd', name='friendipy-xrd'),
                
                #AMCD:
                url(r'^part/amcd/', 'friendipy.views.part_amcd', name='friendipy-part_amcd'),
                
                #OExchange:
                url(r'^part/oexchange/xrd', 'friendipy.views.part_oexchange_xrd', name='friendipy-part_oexchange_xrd'),
                
                #HCard:
                url(r'^part/hcard/(\d+)/$', 'friendipy.views.part_hcard', name='friendipy-part_hcard'),
                
                
                #DFRN protocol urls:
                url(r'^protocol/dfrn/profile/(\d+)/$', 'friendipy.protocols.dfrn.views.show_profile', name='friendipy-protocol_dfrn_show_profile'),
                url(r'^protocol/dfrn/request/(\d+)/$', 'friendipy.protocols.dfrn.views.request', name='friendipy-protocol_dfrn_request'),
                url(r'^protocol/dfrn/confirm/(\d+)/$', 'friendipy.protocols.dfrn.views.confirm_contact', name='friendipy-protocol_dfrn_confirm_contact'),
                url(r'^protocol/dfrn/notify/(\d+)/$', 'friendipy.protocols.dfrn.views.notify', name='friendipy-protocol_dfrn_notify'),
                url(r'^protocol/dfrn/poll/(\d+)/$', 'friendipy.protocols.dfrn.views.send_posts', name='friendipy-protocol_dfrn_send_posts'),
                
                
                #OStatus protocol urls:
                url(r'^protocol/ostatus/add/(\d+)/$', 'friendipy.protocols.ostatus.add_contact', name='friendipy-protocol_ostatus_add_contact'),
                
                )
