from django.apps import AppConfig

class FriendipyConfig(AppConfig):
    name= 'friendipy'
    verbose_name = 'Friendipy social network'
    