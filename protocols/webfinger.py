#
#   This file is part of friendipy
#   Copyright (C) 2015-2016 Moritz Strohm <ncc1988@posteo.de>
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as
#   published by the Free Software Foundation, either version 3 of the
#   License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU Affero General Public License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#


import bs4
import json
import requests


from django.shortcuts import render
from django.http import HttpResponse, HttpResponseBadRequest


class ResourceProperty():
    """
    A class for the self.properties attributes in ResourceDescription.
    """
    def __init__(self, key='', value=''):
        self.key=key
        self.value=value


class ResourceLink():
    
    def __init__(self, rel='', _type='', href=''): #, titles='', properties=''):
        self.rel = rel
        self._type = _type
        self.href = href
        #self.titles = titles
        #self.properties = properties
    
    def __str__(self):
        return "rel="+str(self.rel)+", _type="+str(self._type)+", href="+str(self.href)


class ResourceDescription():
    """
    This is a class for the resource_description.json template.
    Its objects hold all the necessary attributes for a
    WebFinger repsonse as JRD document.
    """
    
    #def __init__(self, subject='', aliases=list(), properties=list(), links=list()):
        #"""
        #FIXME: This constructor is not working:
        #If in probe_host another request is made after the first
        #self.links will contain the links of the first request plus the links
        #of the second request!
        #"""
        #self.subject = subject
        #self.aliases = aliases
        #self.properties = properties
        #self.links = links
        
    
    def __init__(self):
        self.subject = ''
        self.aliases = list()
        self.properties = list()
        self.links = list()
    
    
    #@staticmethod
    #def from_json(json_str=None):
        #"""
        #Parses JSON and return a ResourceDescription object.
        #"""
        #if(not isinstance(json_str, str)):
            #raise Exception("ResourceDescription.from_json requires a JSON string as parameter!")
        
        #json_dict = json.loads(json_str)
        #"""
        #json_dict['subject'] holds the user's account URI (e.g. acct:user@example.org)
        #json_dict['aliases'] is an array with aliases for the account URI
        #json_dict['properties'] is an array of properties for the account
        #json_dict['links'] is an array that holds link objects
        #(with rel, href and optional attributes type, titles, properties)
        #It is optional.
        #Reference: RFC 7033 Section 4.4
        #"""
        #rd = ResourceDescription()
        #rd.subject = json_dict['subject']
        #for a in json_dict['aliases']:
            #rd.aliases.push(a)
        
        #for p in json_dict['properties']:
            #rd.properties.push(ResourceProperty(key=p[0], value=p[1]))
        #for l in json_dict['links']:
            #rd.links.push(ResourceLink(rel=l['rel'], _type=l['type'], href=l['href']))
        
        #return rd
    
    
    def to_dict(self):
        d = dict()
        d['subject'] = self.subject
        d['aliases'] = self.aliases
        d['properties'] = []
        
        for p in self.properties:
            dp = dict()
            dp[p.key] = p.value
            d['properties'].append(dp)
            
        d['links'] = []
        for l in self.links:
            dl = dict()
            dl['rel'] = l.rel
            dl['href'] = l.href
            d['links'].append(dl)
        
        return d


"""
Functions
"""

def probe_host(resource=None):
    """
    resource specifies a resource on a host
    that shall be probed for WebFinger support.
    Returns a ResourceDescription object.
    """
    #print("DEBUG: probe_host: resource="+str(resource))
    resource_parts = resource.split('@',1)
    #print("DEBUG: len(resource_parts)="+str(len(resource_parts))+", resource_parts="+str(resource_parts))
    if(len(resource_parts) == 2):
        """
        The URI has E-Mail syntax. It can be a Friendica or Diaspora profile (via Webfinger)
        or an E-Mail address.
        """
        
        """
        We will first try a standard webfinger request.
        """
        req_url = 'https://'+resource_parts[1]+'/.well-known/webfinger?resource=acct:'+resource
        #print("DEBUG: Trying standard WebFinger request to "+req_url)
        response = requests.get(req_url)
        #print("Got WebFinger response!")
        try:
            print("DEBUG: Trying standard webfinger request!")
            response.raise_for_status() #if this raises an exception then the request failed.
            """
            If the following is executed the request was successful.
            A ResourceDescription object is returned.
            """
            return ResourceDescription() #TODO
            
        
        except Exception as e:
            print("DEBUG: Standard webfinger request failed: "+str(e))
            print("Trying old style webfinger request!")
            """
            The request failed.  We can now check if the server has an old style Webfinger page
            (for Diaspora and Friendica compatibility reasons)
            """
            response = requests.get('https://'+resource_parts[1]+'/.well-known/host-meta') #TODO: HTTPS instead of HTTP
            page_content = bs4.BeautifulSoup(response.text, 'xml')
            xrd_request_url = page_content.select('Link[rel="lrdd"]')[0].get('template')
            if(xrd_request_url != None):
                print("Old style webfinger request successful!")
                """
                The old style webfinger request was successful.
                Now we can get more details to find out what kind of data
                are stored on the target server.
                """
                
                #print(xrd_request_url) #for debug reasons
                
                #we replace the {uri} with the given URI (and we add "acct:" in front of it)
                
                xrd_request_url = xrd_request_url.format(uri='acct:'+resource)
                
                print("looking up "+xrd_request_url+" ...") #for debug reasons
                
                #start another request:
                response2 = requests.get(xrd_request_url)
                page_content2 = bs4.BeautifulSoup(response2.text, 'xml')
                
                """
                Ok, we can now read the content into a ResourceDescription object.
                
                The Friendica/Diaspora WebFinger flavour has just Subject, Alias and Link tags.
                """
                resource_description = ResourceDescription()
                
                try:
                    resource_description.subject = page_content2.find_all('Subject')[0].get_text()
                    #print("subject="+resource_description.subject)
                except Exception as e:
                    raise Exception("Requested profile has no Subject tag in old style webfinger response! Error:"+str(e))
                
                alias_tags = page_content2.find_all('Alias')
                for a in alias_tags:
                    resource_description.aliases.append(a.get_text())
                
                link_tags = page_content2.find_all('Link')
                #resource_description.links = list() #TEST
                for l in link_tags:
                    resource_description.links.append(ResourceLink(rel=l.get('rel'), _type=l.get('type'), href=l.get('href')))
                
                print("DEBUG: rd="+str(resource_description.to_dict())+"\n\n")
                
                return resource_description
            
            """
            If code execution reaches this point then there was no way
            to gather data via WebFinger from the given resource.
            """
            raise Exception("WebFinger request failed!")
    else:
        raise Exception("Resource is not in acct URI format (profile@domain.tld)")

"""
Views
"""

def host_meta(request):
    """
    This exists only for backward compatibility with the Diaspora and Friendica
    implementations of early 2016.
    """
    return render(
        request=request,
        template_name='friendipy/protocols/webfinger/host_description.xml',
        context={
            'domain':request.META['HTTP_HOST'],
            'full_path':request.build_absolute_uri('/')[:-1],
            },
        content_type='text/xml')


def query(request):
    """
    This is responsible for handling all WebFinger requests.
    Depending on the resource parameter different responses
    are sent back to the client.
    """
    
    #First we have to check for the resource parameter (RFC 7033 Section 4.2)
    requested_resource = request.GET.get('resource')
    
    if(requested_resource):
        #the parameter is present: check what kind of resource is requested
        
        #(to be done)
        
        #stub:
        
        resource_description = ResourceDescription(
            subject=request.META['HTTP_HOST'],
            links=[
                ResourceLink(rel='stub', href='http://example.org')
                ]
            )
        
        """
        Since the whole thing is a JSON object there is no need
        to use a template.
        """
        return HttpResponse(
            json.dumps(resource_description.to_dict())+"\n",
            content_type='application/jrd+json')
        
        #return render(
            #request=request,
            #template_name='friendipy/protocols/webfinger/resource_description.json',
            #context={
                #'description':resource_description,
                ##'domain':request.META['HTTP_HOST'],
                ##'full_path':request.build_absolute_uri('/')[:-1],
                #},
            #content_type='application/jrd+json')
    else:
        """
        No "resource" parameter sent: send back HTTP status code 400 (Bad Request)
        (RFC 7033 Section 4.2 and RFC 2616 Section 10.4.1)
        """
        return HttpResponseBadRequest("Webfinger error: resource parameter is missing in request!")
    
    