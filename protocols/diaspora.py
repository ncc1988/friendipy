import bs4
import requests
from datetime import datetime

from friendipy.models import Profile

"""
The server to server protocol for the social network Diaspora is placed in here.

Resources:
- https://wiki.diasporafoundation.org/Federation_protocol_overview

"""

def read_profile(uri=None):
    if(not isinstance(uri, str)):
        raise Exception("Error: read_profile requires an URI string!")
    
    #Diaspora URIs have the form user@server, like E-Mail addresses
    uri = uri.split('@')
    user = uri[0]
    server = uri[1]
    
    #we first have to ask the server where to lookup profiles (get host-meta):
    response = requests.get('https://'+server+'/.well-known/host-meta')
    host_meta_content = bs4.BeautifulSoup(response.text, features='xml')
    
    #TODO: don't rely on Link tag order!
    request_url = host_meta_content.find_all('Link')[0].get('template')
    
    """
    request_url has the form https://server/webfinger?q={uri}
    We must replace {uri} with the uri.
    Fortunately we can use python's string formatting feature for this:
    """
    request_url = str(request_url).format(uri=user+'@'+server)
    #now we can gather further information about a user:
    response = requests.get(request_url)
    
    user_data = bs4.BeautifulSoup(response.text, features='xml')
    #get the profile URL:
    
    #TODO: don't rely on Link tag order!
    profile_url = user_data.find_all('Link')[0].get('href')
    print(profile_url)
    
    #load profile data:
    
    response = requests.get(profile_url)
    profile_data = bs4.BeautifulSoup(response.text, features='xml')
    
    profile = Profile()
    
    profile.url = profile_url
    profile.protocol = 'diaspora'
    profile.last_update = datetime.now()
    profile.name = profile_data.find('span', 'fn').contents[0]
    profile.nickname = profile_data.find('span', 'nickname').contents[0]
    profile.photo_url = profile_data.find('dl', 'entity_photo').find('img').get('src')
    
    #just for testing:
    #print('Diaspora Profile data:')
    #print('Name: '+profile_data.find('span', 'fn').contents[0])
    #print('Nickname: '+profile_data.find('span', 'nickname').contents[0])
    #print('Photo: '+profile_data.find('dl', 'entity_photo').find('img').get('src'))
    #print('URL: '+profile_url)
    
    return profile