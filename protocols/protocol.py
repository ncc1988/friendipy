from django.conf.urls import patterns, url #each protocol has its own urls

"""
This is a pure definition of all methods a protocol has to implement

NOTE: During development this will be left empty. When a new protocol is developed 
all functions placed here have to be implemented in the new protocol.
"""

#the URL definitions of the protocol:
proto_urls = patterns('',
                      
                      )