from friendipy.protocols.dfrn.forms import DfrnRequestForm
from friendipy.pages.main import Page
"""
PAGES
"""
class DfrnRequestPage(Page):
    
    def __init__(self, user=None, profile=None):
        super(DfrnRequestPage, self).__init__(user=user, title='Friendipy DFRN request')
        if(not(isinstance(profile, Profile))):
            raise Exception("DfrnRequestPage: profile is not a Friendipy Profile instance!")
        self.profile = profile
        self.widgets.append(FormWidget(DfrnRequestForm(), action=reverse('friendipy-dfrn-request', profile.id)))
        self.widgets.append(ProfileWidget(profile=profile))
        
