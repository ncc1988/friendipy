
import bs4
import requests
#from datetime import datetime

def read_profile_from_url(url=None):
    #TODO: rename: read_profile(uri=None)
    """
    If an URL is given try to extract as much information as possible from it
    and store it into a Profile instance.
    """
    if(not isinstance(url, str)):
        raise Exception("Error: read_profile_from_url requires an URL string!")
    response = requests.get(url)
    page_content = bs4.BeautifulSoup(response.text)
    profile = Profile()
    
    profile.last_update = datetime.now()
    profile.url = url
    
    profile.protocol = 'friendica/dfrn'
    
    try:
        profile.name = page_content.select('.fn')[0].get_text()
    except Exception:
        print("NOTE: name not defined!")
        profile.name = ''
    try:
        profile.photo_url = page_content.select('.photo')[0].get('src')
    except Exception:
        profile.photo_url = None
    try:
        profile.location = page_content.select('.locality')[0].get_text()
    except Exception:
        profile.location = None
    try:
        profile.country = page_content.select('.country-name')[0].get_text()
    except Exception:
        profile.country = None
    try:
        profile.description = page_content.select('.about')[0].get_text()
    except Exception:
        profile.description = None
    try:
        profile.homepage = page_content.select('.homepage-url')[0].get_text()
    except Exception:
        profile.homepage = None
    return profile


def read_posts_from_url(url=None):
    if(not isinstance(url, str)):
        raise Exception("Error: read_posts_from_url requires an URL string!")
    
    atom_raw = requests.get(url)
    atom_feed = bs4.BeautifulSoup(response.text)
    
    