
#to be renamed to dfrn/widgets.py

from friendipy.core.system import build_url_prefix
from friendipy.models import Profile
from django.template.loader import render_to_string

def write_profile_body(profile=None):
    
    if(not isinstance(profile, Profile)):
        raise Exception("DFRN write_profile_body: profile is not a Friendipy Profile instance!")
    
    return render_to_string('friendipy/protocols/dfrn/Dfrn_profile.html', {
        'profile':profile,
        'url_prefix':build_url_prefix()
        })

def write_header(profile=None):
    """
    The DFRN HTML tags that belong in the HTML head are written here.
    """
    if(not isinstance(profile, Profile)):
        raise Exception("DFRN write_header: profile is not a Friendipy Profile instance!")
    
    return render_to_string('friendipy/protocols/dfrn/Dfrn_head.html', {
        'profile':profile,
        'url_prefix':build_url_prefix()
        })
