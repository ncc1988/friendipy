
from datetime import datetime


class XrdProperty():
    def __init__(self, type_ = None, value = None):
        self.type_ = type_
        self.value = value


class XrdLink():
    def __init__(self, rel = None, type_ = None, href = None, template = None):
        self.rel = rel
        self.type_ = type_
        self.href = href
        self.template = template


class Xrd():
    def __init__(self):
        self.xmlid = None
        self.expiration_date = None #date with UTC time when the document has expired
        self.subject = None
        self.aliases = list()
        self.properties = list()
        self.links = list()
        self.xml_signatures = list()
    
    def add_property(self, type_=None, value=None):
        self.properties.append(XrdProperty(type_ = type_, value = value))
    
    def add_link(self, rel=None, type_=None, href=None, template=None):
        self.links.append(XrdLink(rel=rel, type_=type_, href=href, template=template))
