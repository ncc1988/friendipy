from django.shortcuts import render

from friendipy.models import Profile
from friendipy.core.system import get_domain_name
from friendipy.documents.xrd.functions import profile_to_document

def xrd(request, profile_id=None, domain=None):
    profile_uri = profile_id+"@"+domain
    
    #TODO: GUID support
    
    profile = None
    try:
        profile_id = int(profile_id) #to filter out wrong requests
        profile = Profile.objects.get(pk=profile_id)
    except Profile.DoesNotExist:
        return HttpResponseNotFound("Profile not found!")
    except ValueError as e:
        return HttpResponseBadRequest("Profile ID is invalid!")
    
    #TODO: check profile_domain !!
    
    if(domain != get_domain_name()):
        return HttpResponseBadRequest("Wrong domain name!") #TODO: check if standard compliant!
    
    xrddoc = profile_to_document(profile)
    
    return render(request,
        'friendipy/documents/xrd.xml',
        {
            'document':xrddoc
        })
