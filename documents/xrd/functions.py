from friendipy.models import Profile
from friendipy.documents.xrd.classes import Xrd
from friendipy.core.system import get_domain_name, query_https_enabled
#from django.core.urlresolvers import reverse_lazy


def profile_to_document(profile = None):
    if(not isinstance(profile, Profile)):
        raise Exception("ERROR in profile_to_document: profile is not a Friendipy Profile instance!")
    
    domain = get_domain_name()
    #use_https = query_https_enabled()
    profile_uri = 'acct:'+str(profile.id)+'@'+domain
    
    
    #TODO: fix circular import !!
    #profile_url = ''
    #if(use_https):
    #    profile_url = reverse_lazy('friendipy-profile', profile.id)
    #else:
    #    profile_url = reverse_lazy('friendipy-profile', profile.id)
    
    #profile_url = reverse_lazy('friendipy-profile', profile.id) 
    
    xrddoc = Xrd()
    xrddoc.subject = profile_uri
    xrddoc.aliases.append(profile_uri)
    #xrddoc.aliases.append(profile_url) #TODO: fix circular import
    
    #xrddoc.add_link(rel='http://purl.org/macgirvin/dfrn/1.0', href=profile_url)
    
    
    return xrddoc