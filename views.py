from datetime import datetime

from django.http import HttpResponse, HttpResponseServerError, HttpResponseNotFound, HttpResponseForbidden, HttpResponseRedirect, HttpResponseBadRequest, Http404
from django.core.urlresolvers import reverse
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login as django_login, logout as django_logout

from friendipy.core.profile import get_user_profile, get_profile_settings, read_profile_from_uri
from friendipy.core.themes import get_theme_setting, set_profile_color_scheme

from friendipy.gui import draw_page
from friendipy.core.themes import get_user_theme_name
from friendipy.pages.main import *
from friendipy.pages.settings import SettingsPage, ProfileDataSettingsPage, ProfileThemeSettingsPage
#from friendipy.protocols.dfrn import read_profile_from_url #, show_profile as dfrn_show_profile

from friendipy.forms.profile_forms import BasicProfileEditForm
from friendipy.forms.settings_forms import ProfileThemeSettingsForm
from friendipy.forms.authentication_forms import RegistrationForm
from friendipy.core.authentication import email_domain_allowed
from friendipy.core.contacts import is_in_contact_list

# Create your views here.


def timeline(request):
    if(request.user.is_authenticated()):
        p = TimelinePage(user=request.user)
        #p = draw_page(user=request.user, page='main')
        user_theme = get_user_theme_name(user=request.user)
        return render(request, 'friendipy/themes/'+user_theme+'/FTI_Base.html',
                    {
                        'page':p,
                        'errorMessage':None
                    }
                    )
    else:
        #print("isinstance request.user?" + str(isinstance(request.user, User)))
        p = LoginPage()
        user_theme = 'default'
        return render(request, 'friendipy/themes/'+user_theme+'/FTI_Base.html',
                    {
                        'page':p,
                        'errorMessage':None
                    }
                    )
        #return HttpResponseRedirect(reverse('friendipy-login'))

def login(request):
    """
    Not really a view since it just redirects to other pages.
    """
    try:
        user = authenticate(username=request.POST['username'], password=request.POST['password'])
        if user is not None:
            if user.is_active:
                django_login(request, user)
                return HttpResponseRedirect(reverse('friendipy-timeline'))
            else:
                return HttpResponseForbidden("USER INACTIVE!")
        else:
            return HttpResponseForbidden("INVALID USERNAME OR PASSWORD!")
    except Exception:
        return HttpResponseBadRequest("Invalid request!")


def logout(request):
    """
    Not really a view since it just redirects to other pages.
    """
    django_logout(request)
    return HttpResponseRedirect(reverse('friendipy-timeline'))


def register(request):
    """
    The registration page.
    """
    message_list = []
    if(request.method == 'POST'):
        #the user asks for registration
        registration_form = RegistrationForm(request.POST)
        if(registration_form.is_valid()):
            #do some checks: first check if the e-mail domain is whitelisted or not blacklisted:
            if(email_domain_allowed(registration_form.cleaned_data['email'])):
                #then check if the password and the password confirmation match:
                if(registration_form.cleaned_data['password'] == registration_form.cleaned_data['password_confirmation']):
                    User.objects.create_user(
                        registration_form.cleaned_data['username'],
                        registration_form.cleaned_data['email'],
                        registration_form.cleaned_data['password']
                        )
                else:
                    #password and password confirmation don't match:
                    message_list.append("Error: Password and password confirmation don't match!")
            else:
                #e-mail domain blacklisted or not whitelisted:
                message_list.append('Error: The E-Mail domain from your E-Mail address is blacklisted or not whitelisted! Please use another mail address!')
                
        else:
            message_list.append('ERROR in profile_settings_form!')
        p = RegistrationPage(message_list=message_list)
        return draw_page(request, p)
    else:
        return draw_page(request, RegistrationPage())
    


    
def contacts(request):
    """
    This view shows the add friend page if the request method is GET.
    Otherwise (when the user entered a profile URL) it shows the profile's data.
    """
    if(request.user.is_authenticated()):
        if(request.method == 'POST'):
            #the user asked to add a new friend:
            friend_url = request.POST['profile_url']
            profile = read_profile_from_uri(friend_url)
            p = AddFriendResultPage(user=request.user, profile=profile)
            return draw_page(request, p)
        else:
            return draw_page(request, ContactPage(user=request.user))
    else:
        #user not authenticated: login first
        return HttpResponseRedirect(reverse('friendipy-timeline'))


def show_contact(request, profile_id):
    """
    This is a view to show a contact of a user.
    """
    profile = Profile.objects.get(pk=profile_id)
    profile_page = ContactProfilePage(request.user, profile=profile)
    
    user_theme = get_user_theme_name(user=request.user)
    return render(request, 'friendipy/themes/'+user_theme+'/FTI_Base.html',
                {
                    'page':profile_page
                }
                )
    


def add_contact(request):
    if(request.user.is_authenticated()):
        if(request.method == 'POST'):
            add_friend_form = AddFriendForm(request.POST)
            if(add_friend_form.is_valid()):
                #retrieve profile:
                read_profile = read_profile_from_uri(add_friend_form.cleaned_data['profile_url'])
                #check if that profile is already in the database:
                #TODO: fix this!
                try:
                    existing_profile = Profile.objects.get(url = add_friend_form.cleaned_data['profile_url'])
                    #existing_profile.update(read_profile) #should not throw an exception since profile_url is the same if this is executed
                    profile = existing_profile
                except Profile.DoesNotExist:
                    """
                    Profile does not exist: we can save the profile in the database:
                    """
                    read_profile.save()
                    profile = read_profile
                
                if(not is_in_contact_list(profile=request.user.profile, contact=profile)):
                    #we have to add a contact (and contact group) if it doesn't exist:
                    contact_group = None
                    if(not request.user.profile.contact_groups.all()):
                        #no contact groups present!
                        contact_group = ContactGroup(name='Default group') #STUB
                        request.user.profile.contact_groups.add(contact_group) #STUB
                    else:
                        contact_group = request.user.profile.contact_groups.all()[0] #STUB
                    contact_group.members.add(profile)
                    contact_group.save()
                    
                
                #redirect to contacts page:
                return HttpResponseRedirect(reverse('friendipy-contacts'))
            else:
                return HttpResponseBadRequest('Invalid form!')
        else:
            #HTTP GET: redirect to contacts page
            return HttpResponseRedirect(reverse('friendipy-contacts'))

def show_profile(request, profile_id):
    """
    This is a view for a local user profile that is visible to everyone.
    So we have to display DFRN-information about it.
    The difference between this and timeline is that there is no new post widget,
    a minimal nav bar (login and friendipy logo) and a hidden DFRN profile.
    """
    profile = Profile.objects.get(pk=profile_id)
    profile_page = ProfilePage(request.user, profile=profile)
    
    user_theme = get_user_theme_name(user=request.user)
    return render(request, 'friendipy/themes/'+user_theme+'/FTI_Base.html',
                {
                    'page':profile_page
                }
                )
    
    #get user profile:
    #try:
        #user = User.objects.get(pk=user_id)
        #profile = Profile.objects.get(user=user)
    #except User.DoesNotExist:
        #raise Http404("User not found!")
    #except Profile.DoesNotExist:
        #raise Http404("Profile not found!")
    
    ##profile_page.widgets.append(ProfileWidget(profile))
    
    #return dfrn_show_profile(request, profile)


def raw_profile_photo(request, profile_id):
    return HttpResponseServerError()


def new_post(request):
    if(request.user.is_authenticated()):
        if(request.method == 'POST'):
            #this function should only be called via POST and only be accessible for logged in users
            new_post_form = NewPostForm(request.POST)
            if(new_post_form.is_valid()):
                #the new post form is valid: add a new post
                post = Post()
                try:
                    request.user.profile
                except Profile.DoesNotExist:
                    request.user.profile = Profile()
                    request.user.profile.save()
                    
                post.creator = request.user.profile
                post.title = new_post_form.cleaned_data['title']
                post.body = new_post_form.cleaned_data['content']
                post.creation_date = datetime.utcnow()
                post.save()
            
            return HttpResponseRedirect(reverse('friendipy-timeline'))
        else:
            return HttpResponseRedirect(reverse('friendipy-timeline'))
    else:
        return HttpResponseRedirect(reverse('friendipy-timeline'))


def settings_start(request):
    """
    This view shows the settings start page which lets
    the user decide what settings page to open.
    """
    if(request.user.is_authenticated()):
        p = SettingsPage(user=request.user)
        return draw_page(request, p)
    else:
        return HttpResponseRedirect(reverse('friendipy-login'))

def settings_profile(request):
    """
    This view shows the settings page which allows editing the profile.
    """
    if(request.user.is_authenticated()):
        if(request.method == 'POST'):
            #the user asked to update the profile
            profile_settings_form = BasicProfileEditForm(request.POST, instance=request.user.profile)
            if(profile_settings_form.is_valid()):
                #print("profile_settings_form is valid!")
                profile_settings_form.save()
            #else:
                #print("ERROR in profile_settings_form!")
            p = ProfileDataSettingsPage(user=request.user)
            return draw_page(request, p)
        else:
            return draw_page(request, ProfileDataSettingsPage(user=request.user))
    else:
        #user not authenticated: login first
        return HttpResponseRedirect(reverse('friendipy-login'))


def settings_theme(request):
    """
    This view shows the settings page for configuring themes and color schemes.
    """
    if(request.user.is_authenticated()):
        if(request.method == 'POST'):
            #the user asked to update the profile
            profile = get_user_profile(request.user)
            theme_settings_form = ProfileThemeSettingsForm(request.POST, profile=profile)
            if(theme_settings_form.is_valid()):
                #print("theme_settings_form is valid!")
                """
                Update color scheme:
                """
                set_profile_color_scheme(profile, theme_settings_form.cleaned_data['color_scheme'])
            #else:
                #print("DEBUG: ERROR in theme_settings_form!")
            p = ProfileThemeSettingsPage(user=request.user)
            return draw_page(request, p)
        else:
            return draw_page(request, ProfileThemeSettingsPage(user=request.user))
    else:
        #user not authenticated: login first
        return HttpResponseRedirect(reverse('friendipy-login'))



def part_amcd(request):
    return HttpResponseServerError("Not implemented!")

def part_oexchange_xrd(request):
    return HttpResponseServerError("Not implemented!")

def part_hcard(request, profile_id):
    try:
        profile = Profile.objects.get(pk=profile_id)
    except Profile.DoesNotExist:
        return HttpResponseNotFound("Profile not found!")
    
    return render(request,
        'friendipy/parts/hcard.xml',
        {
            'profile':profile,
            'prefix':request.build_absolute_uri('/')[:-1]
        })
