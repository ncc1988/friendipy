from django import forms

from friendipy.models import Profile

class BasicProfileEditForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ['name', 'nickname', 'location', 'address', 'country', 'description', 'tags']
        
