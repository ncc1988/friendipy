from django import forms

class NewPostForm(forms.Form):
    title = forms.CharField(required=False, label='', widget=forms.TextInput(attrs={'placeholder': "Title – What are you writing about?"}))
    content = forms.CharField(required=True, widget=forms.Textarea(attrs={'placeholder': 'What do you want to share?', 'class': 'RichTextarea'}), label='',)
    