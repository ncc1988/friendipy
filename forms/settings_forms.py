from django import forms

from friendipy.models import Profile, ProfileSettings
from friendipy.core.profile import get_profile_settings
from friendipy.core.themes import get_theme_color_schemes, get_profile_color_scheme, get_profile_theme_name

class ProfileSettingsForm(forms.ModelForm):
    class Meta:
        model = ProfileSettings
        fields = ['theme']
        

class ProfileThemeSettingsForm(forms.Form):
    
    def __init__(self, *args, **kwargs):
        profile = kwargs.pop('profile', None)
        super(ProfileThemeSettingsForm, self).__init__(*args, **kwargs)
        
        if(not isinstance(profile, Profile)):
            raise Exception("ProfileThemeSettingsForm: profile has to be a Friendipy Profile instance!")
        color_scheme_choices_list = get_theme_color_schemes(get_profile_theme_name(profile))
        
        current_color_scheme_name = get_profile_color_scheme(profile) #the user's current color scheme
        
        #make a dict out of the color_scheme_choices_list:
        i = 0
        color_scheme_choices = list()
        for c in color_scheme_choices_list:
            color_scheme_choices.append((i, c))
            i += 1
         
        #print("DEBUG: color_scheme_choices = "+str(color_scheme_choices))
        self.fields['color_scheme'] = forms.ChoiceField(
            choices = color_scheme_choices,
            initial = color_scheme_choices_list.index(current_color_scheme_name)
            )
        
        