from django import forms


class AddFriendForm(forms.Form):
    profile_url = forms.CharField(required=True)
