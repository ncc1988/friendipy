
from friendipy.classes.content import *

class NavItem(object):
    def __init__(self, item_type=None):
        if(item_type != None):
            self.item_type = item_type
        else:
            self.item_type = ''


class NavMenu(NavItem):
    def __init__(self):
        super(NavMenu, self).__init__('Menu')
        self.item_type = 'menu'
        self.entries = [] #list of LinkContent
        self.icon_url = None #this might point to an icon url if possible

class NavLink(NavItem, LinkContent):
    def __init__(self, action=None, title=None, icon_url=None):
        super(NavMenu, self).__init__('Link')
        if(action != None):
            if(title != None):
                super(LinkContent, self).__init__(action=action, title=title, icon_url=icon_url)
            else:
                super(LinkContent, self).__init__(action=action, title=action, icon_url=icon_url)
        

class NavSpacer(NavItem):
    def __init__(self):
        super(NavMenu, self).__init__('Spacer')

