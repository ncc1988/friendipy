# -*- coding: utf-8 -*-

from django import forms
from friendipy.classes.form_elements import *

"""
This module defines content classes for page content.
"""


class Content(object):
    """
    A generic base class for page content.
    """
    def __init__(self, content_type = None):
        if(content_type != None):
            self.content_type = content_type
        else:
            self.content_type = ''
        
        self.events = [] #array of JS events (to be defined)


class TextContent(Content):
    """
    Defines simple textual content.  It may be drawn as <p> or <div>.
    """
    def __init__(self, text=None, size=None):
        super(TextContent, self).__init__('Text')
        if(text != None):
            self.text = text
        else:
            #we want an empty line instead of 'None' if there is no text (caused by missing values or other things)
            self.text = ''
        
        #we have to make sure that only three string values are allowed as size:
        if(isinstance(size, str)):
            if(size == 'big'):
                self.size = 'big'
            elif(size == 'small'):
                self.size = 'small'
            else:
                self.size = 'normal'
        else:
            self.size = 'normal'


class CombinedContent(Content):
    """
    A RowContent is for combining Content elements into one group that shouldn't be separated.
    """
    
    def __init__(self, content=list()):
        self.content = content
        super(CombinedContent, self).__init__('Combined')
        
    
class LinkContent(Content):
    """
    A class for links. Usually displayed as <a> tag with a href attribute (self.action)
    and some text (self.title) in HTML.  If self.icon_url is set the <a> tag will contain
    an <img> tag in HTML.
    """
    def __init__(self, action=None, title=None, icon_url=None):
        super(LinkContent, self).__init__('Link')
        self.action = action
        self.title = title
        self.icon_url = icon_url


class ButtonContent(Content):
    """
    Defines attributes for buttons that can be drawn as <button> or <a> elements in HTML.
    """
    
    def __init__(self, action=None, title=None, icon_url=None, selected=False):
        super(ButtonContent, self).__init__('Button')
        self.action = action
        self.title = title
        self.icon_url = icon_url
        self.selected = selected


class ButtonGroupContent(Content):
    def __init__(self, buttons=list(), align_horizontally = False):
        super(ButtonGroupContent, self).__init__('ButtonGroup')
        self.buttons = buttons
        self.align_horizontally = align_horizontally
        #print("DEBUG: buttons = "+str(self.buttons))


class ImageContent(Content):
    """
    ImageContent is rendered as <img> tag in HTML.
    """
    def __init__(self, source=None, description=None, url=None):
        super(ImageContent, self).__init__('Image')
        self.source=source
        self.description=description
        self.url=url


class MenuContent(Content):
    """
    Menus (also drop-down menus) are represented by this class.
    """
    def __init__(self, title=None, entries=None, icon=None):
        #check if the entries parameter contains a list of LinkContent.
        if(not isinstance(entries, list)):
            raise Exception("Error: MenuContent requires a list of LinkContent objects in the entries parameter!")
        for e in entries:
            if(not isinstance(e, LinkContent)):
                raise Exception("Error: MenuContent requires a list of LinkContent objects in the entries parameter!")
        
        #check if title is set:
        if(not isinstance(title, str)):
            self.title=u' '
        else:
            self.title = title
        
        if(isinstance(icon, ImageContent)):
            #menu icon is set:
            self.icon = icon
        
        self.entries = entries
        super(MenuContent, self).__init__('Menu')
        
        

class DefinitionListContent(Content):
    """
    A class for definition lists that is displayed as <dl> and <dd> tags in HTML.
    
    This is not handled in FTI_Widgets.tpl!!
    """
    def __init__(self, definitions=None):
        super(DefinitionListContent, self).__init__('DefinitionList')
        self.definitions = definitions
        

class FormContent(Content):
    """
    A class for form content.
    """
    def convert_django_form(self, form=None):
        if((not isinstance(form, forms.Form)) and (not isinstance(form, forms.ModelForm))):
            raise Exception("Error: FormContent requires a Django Form or ModelForm class instance!")
        
        self.elements = []
        #convert fields:
        for field in form:
            #let's find out what kind of field it is:
            print('field widget = '+field.widget)
            if(isinstance(field, forms.CharField)):
                self.elements.append(TextElement(name=field.html_name, label=field.label, description=field.help_text, error_html=field.errors, value=field.value))
            elif(isinstance(field, forms.IntegerField)):
                self.elements.append(NumberElement(name=field.html_name, label=field.label, description=field.help_text, error_html=field.errors, value=int(field.value)))
            else:
                #TODO: implement other fields!
                self.elements.append(field)
            
    def __init__(self, form=None, action=None, method=None, submit_value=None):
        if((not isinstance(form, forms.Form)) and (not isinstance(form, forms.ModelForm))):
            raise Exception("Error: FormContent requires a Django Form or ModelForm class instance!")
        
        super(FormContent, self).__init__('Form')
        #self.convert_django_form(form)
        self.form = form
        if(isinstance(action, str)):
            self.action = action
        else:
            self.action = ''
        if(isinstance(method, str)):
            self.method = method
        else:
            self.method = 'post'
        if(isinstance(submit_value, str)):
            self.submit_value = submit_value
        else:
            self.submit_value = 'Save'


class Item():
    def __init__(self, title='', image_url='', text='', action='', tags=None):
        self.title=title
        self.image_url=image_url
        self.text=text
        self.action=action
        self.tags=tags


class ItemContent(Content):
    """
    An ItemContent is a box that contains square fields with a picture on the left side,
    a title, some text and tags.  Plus, it has an action that is executed
    when the image or the title is clicked.
    """
    def __init__(self, items=None):
        super(ItemContent, self).__init__('Items')
        if(isinstance(items, list)):
            self.items = items
        else:
            self.items = []



class HtmlContent(Content):
    """
    This class prints HTML code directly (use it only for quick development or as last resort!).
    """
    def __init__(self, html_code=None):
        super(HtmlContent, self).__init__('Html')
        self.html_code = html_code

class SpaceContent(Content):
    """
    This is just to define empty space between two content elements.
    """
    def __init__(self, size=None):
        super(SpaceContent, self).__init__('Space')
        
        #we have to make sure that only three string values are allowed as size:
        if(isinstance(size, str)):
            if(size == 'big'):
                self.size = 'big'
            elif(size == 'small'):
                self.size = 'small'
            else:
                self.size = 'normal'
        else:
            self.size = 'normal'