
from django.core.urlresolvers import reverse
from django import forms

from friendipy.classes.content import *

class Widget(object):
    def __init__(self, widget_type = None, title=None, description = None, content = None, layer=0):
        if(widget_type != None):
            self.widget_type = widget_type
        else:
            self.widget_type = ''
        self.title = title #may be unset
        self.description = description #may be unset
        self.layer = layer
        self.child_widgets = []
        if(content != None):
            self.content = content #list of Content
        else:
            self.content = list()


class FormWidget(Widget):
    """
    This combines the standard Django forms with the Friendipy Widget class.
    """
    def __init__(self, form=None, action=''):
        if((not isinstance(form, forms.Form)) and (not isinstance(form, forms.ModelForm))):
            raise Exception("Error: FormWidget requires a Django Form or ModelForm class instance!")
        super(FormWidget, self).__init__(widget_type='Form')
        
        self.content.append(FormContent(form=form, action=action))
