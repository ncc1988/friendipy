
class FormElement():
    def __init__(self, name=None, label=None, description=None, error_html=None):
        if(isinstance(name, str)):
            self.name = name
        else:
            self.name = ''
        if(isinstance(label, str)):
            self.label = label
        else:
            self.label = ''
        if(isinstance(description, str)):
            self.description = description
        else:
            self.description = ''
        if(isinstance(error_html, str)):
            self.error_html = error_html
        self.element_type = ''


class TextElement(FormElement):
    def __init__(self, name=None, label=None, description=None, error_html=None, value=None):
        super(FormElement, self).__init__(name, label, description, error_html)
        if(isinstance(value, str)):
            self.value = value
        else:
            self.value = ''
        self.element_type = 'Text'


class NumberElement(FormElement):
    def __init__(self, name=None, label=None, description=None, error_html=None, value=None):
        super(FormElement, self).__init__(name, label, description, error_html)
        if(isinstance(value, int)):
            self.value = value
        else:
            self.value = 0
        #TODO: decimal separator, max, min, ...
        self.element_type = 'Number'
    