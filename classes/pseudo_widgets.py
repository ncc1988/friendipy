from django.core.urlresolvers import reverse

from friendipy.models import *
from friendipy.classes.widgets import *
from friendipy.core.contacts import get_profile_contacts

#from friendipy.protocols.dfrn import write_profile_body as dfrn_write_profile_body
from friendipy.protocols.dfrn.widgets import write_profile_body as dfrn_write_profile_body

class ProfileWidget(Widget):
    """
    This is a special widget to display profile information.
    It uses normal content elements.
    """
    def __init__(self, profile, show_protocol_profiles=False):
        """
        If show_protocol_profiles is set to True, hidden profiles for some protocols (e.g. DFRN)
        are added to this page.
        """
        if(profile == None):
            raise Exception("Error: ProfileWidget requires a Friendipy Profile class instance!")
        profile_content = []
        if(profile.nickname):
            profile_content.append(TextContent(text=str(profile.nickname)+' ('+str(profile.protocol)+')', size='small'))
        else:
            profile_content.append(TextContent(text='('+str(profile.protocol)+')',size='small'))
        profile_content += [
            ImageContent(source=profile.photo_url, description = profile.name), #TODO: url=$PROFILE_URL (via reverse)
            TextContent(text=profile.gender),
            TextContent(text=profile.date_of_birth), #TODO: rename in DB: date_of_birth
            TextContent(text=profile.address)
            ]
        if((profile.location != None) and (profile.country != None)):
            profile_content.append(TextContent(text=profile.location+', '+profile.country))
        elif(profile.location != None):
            #no country set:
            profile_content.append(TextContent(text=profile.location))
        else:
            #no location set:
            profile_content.append(TextContent(text=profile.country))
        if(profile.homepage != None):
            profile_content.append(LinkContent(action=profile.homepage, title=profile.homepage))
        #the description is below the photo:
        profile_content.append(TextContent(text=profile.description, size='small'))
        
        if(show_protocol_profiles == True):
            #the hidden DFRN profile belongs here:
            profile_content.append(HtmlContent(html_code=dfrn_write_profile_body(profile)))
        
        super(ProfileWidget, self).__init__(title=profile.name, content=profile_content)


class ContactItemsWidget(Widget):
    """
    This widget displays a list of contacts as square items
    with pictures of the contacts.
    """
    def __init__(self, user):
        if(not isinstance(user, User)):
            raise Exception("Error: ContactItemsWidget requires a Django User class instance!")
        
        #we load the user's contacts:
        contacts = get_profile_contacts(user.profile)
        if(contacts):
            #there is at least one contact
            item_list = ItemContent()
            for contact in contacts:
                item_list.items.append(Item(title=contact.nickname,
                                            image_url = contact.photo_url,
                                            text = contact.name,
                                            action = '/contact/'+str(contact.id), #TODO: keep in sync with URL friendipy-show_contact
                                            tags = contact.tags.all()
                                            ))
            super(ContactItemsWidget, self).__init__(title='Your contacts', content=[item_list])
        else:
            super(ContactItemsWidget, self).__init__(title='Your contacts', content=[TextContent(text="You don't have any contacts!")])


class PostWidget(Widget):
    """
    This is a widget to display a post or post comments.
    """
    
    def __init__(self, post):
        if(not isinstance(post, Post)):
            raise Exception("Error: PostWidget requires a Friendipy Post class instance!")
        
        #let's determine the post's layer:
        layer = 0
        try:
            parent = post.parent
            while(parent != None):
                layer += 1 #increment the layer counter
                parent = parent.parent #go up one layer
        except Post.DoesNotExist:
            #if post.parent does not exist we have a post (and no comment): layer 1
            layer = 1
        
        content = [
            CombinedContent([
                LinkContent(action=reverse('friendipy-profile', args=[post.creator.id]), title=post.creator.name),
                TextContent(post.title, size='big')
                ]),
            TextContent(post.body) #TODO: decode BBCode first and change to HTML content
            ]
        
        super(PostWidget, self).__init__(title=None, layer=layer, content=content)
        
        #add threads (and their widgets):
        for thread in post.threads.all():
            self.child_widgets.append(PostWidget(thread))
            
        
class SettingsPagesWidget(Widget):
    """
    This is a widget especially for the settings pages. It defines an index in the side bar
    to allow users to quickly jump to specific settings.
    """
    
    def __init__(self):
        
        buttons = [
            ButtonContent('/settings/profile', 'Profile settings'), #TODO: keep in sync with URL friendipy-settings_profile
            ButtonContent('/settings/theme', 'Theme settings'), #TODO: keep in sync with URL friendipy-settings_theme
            ]
        button_group = ButtonGroupContent(buttons=buttons)
        super(SettingsPagesWidget, self).__init__(content=[button_group])
        