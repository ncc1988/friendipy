
from friendipy.classes.content import *
from friendipy.classes.nav import *
from friendipy.classes.widgets import *
from friendipy.core.themes import get_user_theme_name
from django.contrib.auth.models import User
from django.shortcuts import render
from friendipy.pages.main import Page

"""
This file handles all the GUI stuff: creating widgets etc...
"""



def draw_page(request=None, page=None, error_message=None):
    """
    This function is responsible for drawing the HTML code from a page that was given to it.
    """
    if(request == None):
        #TODO: check for class instance!
        raise Exception("Error: draw_page needs a valid request object!")
    if(not isinstance(page, Page)):
        raise Exception("Error: draw_page needs a Page object!")
    user_theme = get_user_theme_name(user=request.user)
    
    return render(request, 'friendipy/themes/'+user_theme+'/FTI_Base.html',
                        {
                            'page':page,
                            'error_message':error_message
                        }
                        )